#!/bin/sh
## Xe1phix-IRC-Guide-Version2-2.5.sh


# This is the sshd server system-wide configuration file. 
# This sshd was compiled with 
    PATH=/usr/bin:/bin:/usr/sbin:/sbin 
# The strategy used for options in the default sshd_config 

# shipped with OpenSSH is to 
# specify options with their default value where possible, 


# but leave them commented. Uncommented options override the  default value. 

# ============================== WHAT IS THIS ? ==============================
# 
# This is my default setup when it comes to setting up a new server and ssh.
# A lot of these settings are further customized for the server and setup at
# hand so make sure look into what you need for your use case. I run on Arch
# Linux so some of the settings here might not be the same for you if you are
# using some other SSH service.
# Here's a quick rundown of the settings and methods I use:
# - Enforce public key authentication
# - Disable root login and lock the account
# - Plain text login attempts can lead to bans
# - 2 Factor Authentication is mandatory
# - SFTP requires the same authentication methods
# ============================= CUSTOM SETTINGS ==============================
# 
# - Connection options. 
# The most important being the port. -
# Port 32641
# AddressFamily any
# ListenAddress 0.0.0.0
# ListenAddress ::
# - Custom host key settings. -
# # HostKey /etc/ssh/ssh_host_rsa_key
# HostKey /etc/ssh/ssh_host_dsa_key
# HostKey /etc/ssh/ssh_host_ecdsa_key
# HostKey /etc/ssh/ssh_host_ed25519_key
# 
# - We ignore enforcing strict policies 
# as fail2ban will handle the most. -
# # LoginGraceTime 2m
# StrictModes yes
# MaxAuthTries 6
# MaxSessions 10
# - Log additional information such as failed logins. -
# LogLevel VERBOSE
# - Enable public key authentication. -
# PubkeyAuthentication yes

# The default is to check both 
#   .ssh/authorized_keys and .ssh/authorized_keys2
#
# but this is overridden so installations will only check 
#   .ssh/authorized_keys AuthorizedKeysFile .ssh/authorized_keys
#
# > To disable tunneled clear text passwords, change to no here! -
# 
#   PasswordAuthentication no 
#   PermitEmptyPasswords no
#
# - Disable logging in as root. -
#   
#   PermitRootLogin no
# 
# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. 
# If this is enabled, PAM authentication will be allowed through the
# ChallengeResponseAuthentication
# PasswordAuthentication
# Depending on your PAM configuration,
# PAM authentication via 
#   ChallengeResponseAuthentication 
# may bypass the setting of 
#   "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# 
# PAM authentication yes
# PasswordAuthentication
# ChallengeResponseAuthentication no
# UsePAM yes
#
# - Disable key passwords. 
#   Enabling is required for 2FA. -
# ChallengeResponseAuthentication yes
# - Authentication order. 
#   First use public key then ask for 2FA. -
# AuthenticationMethods 
#   publickey,keyboard-interactive:pam
# - Override default of no subsystems -
# Subsystem 
#   sftp /usr/lib/ssh/sftp-server
# - Disable printing the MOTD as PAM does that. -
# PrintMotd no
# - Set default banner path. -
# Banner /etc/banner
# ======================= ADDITIONAL UNTOUCHED OPTIONS =======================
# 
# - Ciphers and keying -
# 
# RekeyLimit default none
# - Logging -
# 
# SyslogFacility AUTH
# AuthorizedPrincipalsFile none
# AuthorizedKeysCommand none
# AuthorizedKeysCommandUser nobody
# - For this to work you'll also need host keys in /etc/ssh/ssh_known_hosts -
# 
# HostbasedAuthentication no
# - Change to yes if you don't trust ~/.ssh/known_hosts -
# 
# HostbasedAuthentication
# IgnoreUserKnownHosts no
# - Don't read the user's ~/.rhosts and ~/.shosts files -
# 
# IgnoreRhosts yes
# Kerberos options
# KerberosAuthentication no
# KerberosOrLocalPasswd yes
# KerberosTicketCleanup yes
# KerberosGetAFSToken no
# GSSAPI options
# GSSAPIAuthentication no
# GSSAPICleanupCredentials yes
# AllowAgentForwarding yes
# AllowTcpForwarding yes
# GatewayPorts no
# X11Forwarding no
# X11DisplayOffset 10
# X11UseLocalhost yes
# PermitTTY yes
# PrintLastLog yes
# TCPKeepAlive yes
# UseLogin no
# PermitUserEnvironment no
# Compression delayed
# ClientAliveInterval 0
# ClientAliveCountMax 3
# UseDNS no
# PidFile /run/sshd.pid
# MaxStartups 10:30:100
# PermitTunnel no
# ChrootDirectory none
# VersionAddendum none
# - Example of overriding settings on a per-user basis -
# # Match User anoncvs 
#	 X11Forwarding no 
#	 AllowTcpForwarding no 
#	 PermitTTY no 
#	 ForceCommand cvs server


## For the purpose of this guide I will focus on typing commands 
## as this way is compatible with both Hexchat and irssi.

## Hexchat is a GUI client, 
## configuration is done by through hexchats gui

## Irssi is a more traditional unix client 
## where interaction is done only by keyboard

 
## Step 1, Registration: 

## You have to type these in order to register.

## 1. Join a network: 
    /server -ssl <irc.network_name.net> 6697

## 2. Pick a nickname that is free 
## (you'll have your nick changed until you choose one free): 
      /nick <your_nickname>

## 3. Request nickname registration (of your current nickname): 
      /msg NickServ REGISTER <your_password> <your@email.com>


## 4. Check your e-mail and confirm:

## >On Freenode: 
    /msg NickServ VERIFY REGISTER <your_nickname> <confirmation_code_from_email>

## >On Rizon: 
    /msg NickServ CONFIRM <confirmation_code_from_email>


## 5. Hide your e-mail:

## >On Freenode: 
    /msg nickserv set hidemail on

## >On Rizon: 
    /msg nickserv set hide email on



## 6. Conceal yourself:

## >On Freenode
## -Check for available staff: 
    /stats p

## -Message an available staff: 
    /msg <nickname_of_staff> Hello, may I have an unaffiliated cloak?

## -Wait until they give you the cloak.

## >On Rizon

## -Request a vHost 
## (pick an appropriate vhost): 
    /msg HostServ REQUEST <vhost.goes.here>

## -Wait until staff confirms.

## -When confirmed type: 
    /msg HostServ ON
 
 

## Step 2, Configure your client (choose one):

## Option 1, SASL:

## >On Hexchat:

## -Open the Network List 
    (Ctrl + S)

## -Find each network to connect in the list 
## then click on Edit to enter the Edit menu

## -On the Edit menu 
## check "Use SSL..." 
##       and 
## "Accept invalid SSL"

## -On the Edit menu 
## uncheck the use of global user identification

## -On the Edit menu 
## enter your nick in the Nick name and User name field

## -On the Edit menu 
## select SASL (username + password) 
## in the Login method field

## -On the Edit menu 
## enter your password in the password field



## >On irssi:


## change your nick and
## match the network nick 
## this allows for auto registration
 
## -First Edit irssi's config file: 
     ~/.irssi/config 


## -Inside irssi type:
    /server add -auto -net <network_name> -ssl -ssl_verify <irc.network_name.net> 6697

    /network add -sasl_username <your_nickname> -sasl_password <your_password> -sasl_mechanism PLAIN <network_name>

    /save


## Option 2, CertFP:
## Open a command line and type 
## (you will be asked some questions, 
## the importance of filling these correctly may vary):

    openssl req -x509 -newkey rsa:4096 -sha256 -days 365 -nodes -keyout Certificate.key -out Certificate.crt

    cat Certificate.crt Certificate.key > Certificate.pem
    rm Certificate.crt Certificate.key

## Before jumping to each client, 
## note that for both clients 
## the network and hostname should match 
## (including the lower and uppercase) 
## to what you already use.


## >On HexChat:

## -Rename the pem file to the network you wish 
    <Network_name>.pem

## Then move it to 
    ~/.config/hexchat/certs/

## -Open the Network List 
    (Ctrl + S)

## -Find each network to connect in the list 
## then click on Edit to enter the Edit menu

## -On the Edit menu select the boxes 
## "Use SSL for all the servers on this network" 
## and "Accept invalid SSL certificate"

## -On the Edit menu select 
## SASL EXTERNAL (cert) 
## in the Login method field

## -Connect to the network: 
    /server -ssl <network_name> 6697

## -Login: 
    /msg NickServ IDENTIFY <your_password>

## -Register your fingerprint 
## (append FINGERPRINT if on Rizon): 
    /msg NickServ cert add

## >On irssi:

## -Rename the pem file to the network you wish 
## (<network_name>.pem) 


## and move it to 
    ~/.irssi/certs/
## -Edit 
    ~/.irssi/config 
## to change your nick and match the network nick to make auto registration

## -Remove the server 
## (make sure to remove all instances): 
    /server remove <irc.network_name.net>

## -Add it again with the SSL flag: 
    /server add -ssl -ssl_cert ~/.irssi/certs/<network_name>.pem -network <network_name> <irc.network_name.net> 6697

## -Connect to the network 
## (do not /reconnect): 
    /connect <network_name>

## -Login: 
    /msg NickServ IDENTIFY <your_password>

## -Register your fingerprint 
    /msg NickServ cert add

## if on Rizon use 
   /msg NickServ access

## -Save changes: 
   /save


## Option 3, Use a free bouncer. 
## Choose a network address 
## (<irc.network_name.net>) 
## from http://wiki.znc.in/Providers
## and use steps from a previous option you wish.
 
 

## Step 3 (OPTIONAL), 


## set ZNC if you have a home server or a VPS. 
## During the configuration wizard you are prompt to some options, 
## if is not mentioned here is safe to leave blank except for the modules 
## so first check the modules you want. 
## If you want a web interface to control ZNC on global modules 
## choose the "webadmin" option:


## 0. OPTIONAL. 
## Make sure you have 
## libicu-dev for unicode characters: 
## <package_manager_installation_command> libicu-dev


## 1. Create a user for ZNC: 
      adduser znc

## 2. Switch to that user: 
      su znc

## 3. Change to the user's home directory: 
      cd ~

## 4. Install ZNC on your server. 




## OPTIONAL 

## manual installation from source

## -Download: 
    wget -c http://znc.in/releases/znc-latest.tar.gz

## -Untar: 
    tar -xzvf znc-latest.tar.gz

## -Change to the unziped directory: 
    cd znc-<version>

## -Configure installation and 
##  path (choose whatever but preferably use "$HOME/.bin": 
      ./configure --prefix="</path/to/znc/program>"

## -Make: 
    make

## -Install: 
    make install


## 5. Change back to znc home direcoty: 
      cd ~

## 6. Remove files you don't need: 
      rm -r znc-*

## 7. Change to where znc is installed: 
      cd </path/to/znc/program>

## 8. Launch the configuration wizard: 
      ./znc --makeconf

## 9. Add a port to run ZNC on 
## (don't run in anything less than 1024): 
      <znc_port_number>

## 10. If you know you have openssl 
## choose to connect with SSL to your server 
## (you need this for certauth)

## 11. Choose if you want to connect with ipv6 to your server 
        "no"

## 12. Add a user name for login to ZNC: 
        <znc_username>

## 13. Add a password for that login: 
        <znc_password>

## 14. Choose if you want that login be an administrator of ZNC
        

## 15. Add your IRC username: 
        <your_nickname>

## 18. When asks to setup a new server choose 
        "yes"

## 19. Add IRC server address:  
        <irc.network_name.net>

## 20. Add a port to connect to the network, should be 

        6697 

## unless otherwise specified

## 21. When done with the configuration 
## exit the znc user just by pressing 
    CTRL-d 
## or you can type 
    "exit"

## 22. While still on the server 
## add a cron job to autostart ZNC 
## (if these don't work you are not using vixie cron)


## -Start the cron daemon:

## >With System V: 
    sudo update-rc.d cron enable

## >With systemd: 
    sudo systemctl enable cron.service

## -Add a cron job: 
    sudo crontab -e -u znc

##          -Add the lines 
## if you DIDN'T do the manual install 
##       the location should be:

    /usr/local/bin/znc 

    @reboot </path/to/znc/program> >/dev/null 2>&1

## -If you use ZNC only in your local server 
## add these lines too to set a tight firewall:
    sudo iptables -I INPUT -p tcp --dport <znc_port_number> -s 192.168.18.0/24 -j ACCEPT
    sudo iptables -A INPUT -p tcp --dport <znc_port_number> -j DROP

## 23. Configure authentication of the irc client:





## >On Hexchat:

## -Open the Network List 
    (Ctrl + S)


## -Create a new network in the list 
## then click on Edit 
## to enter the Edit menu

## -On the servers list 
## add you ZNC server address 
## followed by the port: 
<znc_server_ip>/<znc_port_number>


## -On the Edit menu check
        "Use SSL..." 
##         and 
    "Accept invalid SSL"


## -On the Edit menu 
## uncheck the use of 
## global user identification

## -On the Edit menu enter 
    <znc_username>/<network_name> 
## in the User name field


## -On the Edit menu select SASL 
    (username + password) 
##   in the Login method field


## -On the Edit menu enter your 
        <znc_password> 
## in the password field




## >On irssi:

##         -Remove the server 
## (make sure to remove all instances): 
    /server remove <irc.network_name.net>

## -Add the ZNC server in its place. 
## NOTE: You can use another name 
## for the network and leave the normal name
## o connect directly without the bouncer 
## such as znc_network_name:
    /network add <znc_network_name>
    /server add -net <znc_network_name> -ssl <znc_server_ip> <znc_port_number> <znc_username>/<network_name>:<znc_password>
    /save


## -Connect to ZNC pointing to the network: 
    /connect <znc_network_name>

## -Login as you normally would 
##      do this for each network 
## (If you want ZNC to stay connected): 
    /msg NickServ IDENTIFY <your_password>


## 24. Choose some modules to load 
## (some are required for further options)


## -List all available modules: 
    /msg *status ListAvailMods

## -Load modules: 
    /msg *status LoadMod <znc_module_name>




## important modules:


## ----------------------------------------------------------------------------------------- ##
  "chansaver":           | ZNC config up to date with channels you parted/joined 
                          | so that you don't have to add them manually
## ----------------------------------------------------------------------------------------- ##
    "dcc" and  :          | transfer files to and from ZNC, 
    "bouncedcc":          | while using ZNC as a middle man
## ----------------------------------------------------------------------------------------- ##
  "controlpanel":        | edit ZNC configuration through your IRC client
## ----------------------------------------------------------------------------------------- ##
  "perform":             | keep a list of commands to be executed
 ## ----------------------------------------------------------------------------------------- ##
  "sasl":                | authenticate to an IRC network with SASL, 
## ---------------------- | you will also need this for authenticating with a certificate
## ----------------------------------------------------------------------------------------- ##
  "cert":                | authenticate to an IRC network with a certificate
 
  "certauth":            | required (ADVISED) to use a certificate 
                          | and hide your password from plain files




## 25. OPTIONAL. 


## If you want to connect with CertFP (ADVISED 
## as it will hide your password), 
## load the certauth module


## -Copy your pem file to the server 
## running ZNC and to  
    /home/znc/.znc/users/<znc_username>/networks/<network_name>/moddata/cert/user.pem

## -Add your fingerprint to the ZNC server:  
    /msg *certauth add

## -Restart the ZNC server:  
    /msg *status Restart





## >On HexChat:

## -Rename the pem file to the network you wish 
    (<znc_network_name>.pem) 

## move it to        
     ~/.config/hexchat/certs/

## -Open the Network List  
    (Ctrl + S)

## -Create a new network in the list 
## then click on Edit to 
## enter the Edit menu

## -On the servers list 
## add your ZNC server address 
## followed by the port:  
    <znc_server_ip>/<znc_port_number>

## -On the Edit menu 
## select the boxes 

## "Use SSL for all the servers on this network" 
##              and 
## "Accept invalid SSL certificate"

## -On the Edit menu 
## uncheck the use of global user identification


## -On the Edit menu 
## enter <znc_username>/<network_name> 
## in the User name field

## -On the Edit menu 
## select SASL EXTERNAL (cert) 
## in the Login method field


## -On the Edit menu enter your  
    <znc_password> 
## in the password field


##     -Connect to ZNC 
## pointing to the network:  
    /server -ssl <znc_network_name> <znc_port_number>




## >On irssi:

## -Remove the server 
## (make sure to remove all instances):  
    /server remove <znc_server_ip>

## -Add the server pointing to the pem file 
## and without the password:  
    /server add -ssl -ssl_cert ~/.irssi/certs/<znc_network_name>.pem  -net <znc_network_name> <znc_server_ip> <znc_port_number> <znc_username>/<network_name>:
/save

## -Connect to ZNC pointing to the network:  
    /connect <znc_network_name>

## 25. Confirm fingerprint for each network 
## in case of using CertFP 
## (requires "cert" module loaded):

## -Login:  
    /msg NickServ IDENTIFY <your_password>

## >FREENODE

## -To look for you fingerprint on FREENODE:  
    /msg NickServ cert list

## -Connect to FREENODE: 
    /msg NickServ cert add <fingerprint>


## >RIZON

## -To look for you fingerprint on RIZON: 
/msg NickServ access list


## -Connect to RIZON:  
    /msg NickServ access add fingerprint <fingerprint>


## NOTES:

## -To join the ZNC server 
## if you set ZNC 
## but not your client: 
    /server -ssl <znc_server_ip> <znc_port_number> <znc_username>:<znc_password>

## -To join the ZNC server 
##  (if you set ZNC and certauth, 
##  but not your client): 
    /server -ssl <znc_server_ip> <znc_port_number> <znc_username>:

## -To join the ZNC server 
## if you set ZNC and your client:  
    /connect <znc_network_name>
 
 
 

## Step 4, Learn some commands: 

## Anything that starts with a forward slash is a command. 
## To login you need to either type these each time 
## or configure your client to do it for you. 
## To connect on Hexchat you can go to network list. 
## Whenever you enter IRC you connect to a network 
## but you are not immediately connected to a channel.


## To see the help from your IRC client 
## (some_command is optional): 
    /help <some_command>

## To see network specific help 
## (while in that networks window 
## -<some_command> is optional-): 
    /msg NickServ help <some_command>

## To join a network: 
    /server -ssl <irc.network_name.net> 6697

## To join a network 
## (after you set your client): 
    /server <network_name>

##        To join a network 
## without leaving the current network 
##           (on irssi): 
    /connect <network_name>

## To quit a network 
## (while in a window from that network): 
    /discon

## To login your nickname: 
    /msg NickServ IDENTIFY <your_password>

## To join a channel: 
    /join #<some_channel>

## To quit a channel 
## (when you are in that channel): 
    /part

## To send a message to a single individual: 
    /msg <someone's_nickname> <some_message>

## To open a chat window with a single individual: 
    /query <someone's_nickname>

## To close a chat window of a single individual 
## (in the window of that individual): 
    /q

## To send files to someone: 
    /DCC send <someone's_nickname> </path/to/file>

## To accept files from someone: 
    /DCC get <someone's_nickname>

## To describe yourself in third person 
## (mostly describing an action): 
    /me <does_something>
 
 

## Step 5, LURK! 


## I see people join an IRC channel, say something, and then leave 2 minutes later 
## because they havent been replied to yet. 
## The speed of IRC conversations can be realtime, 
## but it can also be much slower than that sometimes. 
## You should be willing to leave your IRC program running 24/7 
## so you can participate in longer-timescale conversations. 
## This is also the best way to guarantee that you get questions answered.
